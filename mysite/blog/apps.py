from django.apps import AppConfig
#from django.apps import TaggitAppConfig

class BlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'blog'

#BlogConfig = TaggitAppConfig(BlogConfig)
